package workshop.hmsk.id.workshop;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String username;
    String password;

    EditText etUsername;
    EditText etPassword;
    Button bLogin;
    TextView tvRespon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etUsername = findViewById(R.id.et_username);
        etPassword = findViewById(R.id.et_password);
        bLogin = findViewById(R.id.b_login);
        tvRespon = findViewById(R.id.tv_pesan);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();

                Toast.makeText(MainActivity.this, username+password, Toast.LENGTH_LONG).show();
                if (username.equals("admin")){
                    if (password.equals("123456")){
//                        tvRespon.setText("Login Sukses");
                        Intent intent = new Intent(MainActivity.this,
                                LoginResponse.class);
                        startActivity(intent);
                    } else {
                        tvRespon.setText("Username atau password salah");
                    }
                } else {
                    tvRespon.setText("Username atau password salah");
                }
            }
        });
    }
}
